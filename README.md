# 钉钉工具箱 - dingtalk-toolbox

## 安装
### 1. 使用composer安装
```
composer require unhan/dingtalk-toolbox
```

## 使用
### 1、钉钉自定义机器人接入
#### 1.1 使用use引入DingTalkCustomRobotAccess
```
use unhan\DingtalkToolbox\CustomRobotAccess\DingTalkCustomRobotAccess;
```

#### 1.2 使用方法
##### 1.2.1 发送自定义消息
```
$accountToken = '自定义机器人的accountToken';
$data = [
    'msgtype' => 'text',
    'text' => [
    'content' => '测试' . time(),
],
'at' => [
    'atMobiles' => [
        '135****5302',
    ],
    'atUserIds' => [
        '123',
    ],
        'isAtAll' => false,
    ],
];
$result = DingTalkCustomRobotAccess::init($accountToken)->sendMsg($data);
```
##### 1.2.2 发送文本类型消息
```
$accountToken = '自定义机器人的accountToken';
$content = '测试发送消息';
$atMobiles = [
    '135****5302',
];
$atUserIds = [
    '123',
];
$isAtAll = true;
$result = DingTalkCustomRobotAccess::init($accountToken)->sendTextMsg($content, $atMobiles, $atUserIds, $isAtAll);
```
##### 1.2.3 发送markdown类型消息
```
$env = 'test';
$title = '消息标题';
$content = '错误信息，文件位置，报错行数，' . time();
$text = "### **报警名称：** " . $title . "\r\n ### **所属坏境：** " . $env . "\r\n- **异常信息：** " . $content . "\r\n- **请owner尽快处理！**";
$atMobiles = [
    '135****5302',
];
$atUserIds = [
    '123',
];
$isAtAll = true;
$result = DingTalkCustomRobotAccess::init($accountToken)->sendMarkdownMsg($title, $text, $atMobiles, $atUserIds, $isAtAll);
```
- #### 返回示例
```
// 返回成功示例
$result = [
    "errcode" => 0
    "errmsg" => "ok"
];
// 返回失败示例
$result = [
    "errcode" => 310000
    "errmsg" => "ip xxxxxx not in whitelist, more: [https://ding-doc.dingtalk.com/doc#/serverapi2/qf2nxq]"
];
```

### 文档：

1.钉钉自定义机器人文档：https://open.dingtalk.com/document/robots/custom-robot-access
