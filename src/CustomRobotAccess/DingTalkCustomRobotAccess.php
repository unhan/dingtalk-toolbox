<?php

namespace unhan\DingtalkToolbox\CustomRobotAccess;

use unhan\DingtalkToolbox\Exception\DingTalkException;

/**
 * 自定义机器人接入
 * Class DingTalkCustomRobotAccess
 * @package unhan\DingtalkToolbox\CustomRobotAccess
 * 帮助文档 https://open.dingtalk.com/document/robots/custom-robot-access
 */
class DingTalkCustomRobotAccess
{

    /**
     * 请求地址
     * @var string
     */
    protected $url = "https://oapi.dingtalk.com/robot/send?access_token=";

    /**
     * 请求accessToken
     * @var string
     */
    protected $accessToken = '';

    /**
     * 发送消息类型
     */
    public const MSG_TYPE_TEXT = 'text';
    public const MSG_TYPE_MARKDOWN = 'markdown';

    /**
     * @param $accessToken
     * @return DingTalkCustomRobotAccess
     */
    public static function init($accessToken)
    {
        $self = new self();
        $self->accessToken = $accessToken;
        return $self;
    }

    /**
     * 发送自定义消息
     * @param array $data
     * @return mixed
     */
    public function sendMsg(array $data)
    {
        $url = $this->url . $this->accessToken;
        $header = [
            'Content-Type: application/json;charset=utf-8',
        ];
        if (is_array($data)) {
            $data = json_encode($data);
        }
        if (empty($data)) {
            throw new DingTalkException("请求参数不能为空");
        }
        $result = $this->curlRequest($url, $header, $data);
        return $result;
    }

    /**
     * 发送文本类型消息
     * @param string $content 消息内容
     * @param array $atMobiles 被@人的手机号
     * @param array $atUserIds 被@人的用户userid
     * @param bool $isAtAll 是否@所有人
     * @return mixed
     */
    public function sendTextMsg(string $content, array $atMobiles = [], array $atUserIds = [], bool $isAtAll = false)
    {
        $data = [
            'msgtype' => static::MSG_TYPE_TEXT,
            'text' => [
                'content' => $content,
            ],
        ];
        if (!empty($atMobiles)) {
            $data['at']['atMobiles'] = $atMobiles;
        }
        if (!empty($atUserIds)) {
            $data['at']['atUserIds'] = $atUserIds;
        }
        if (true === $isAtAll) {
            $data['at']['isAtAll'] = $isAtAll;
        }
        return $this->sendMsg($data);
    }

    /**
     * 发送markdown类型消息
     * @param string $title 首屏会话透出的展示内容
     * @param string $text markdown格式的消息
     * @param array $atMobiles 被@人的手机号
     * @param array $atUserIds 被@人的用户userid
     * @param bool $isAtAll 是否@所有人
     * @return mixed
     */
    public function sendMarkdownMsg(string $title, string $text, array $atMobiles = [], array $atUserIds = [], bool $isAtAll = false)
    {
        $data = [
            'msgtype' => static::MSG_TYPE_MARKDOWN,
            'markdown' => [
                'title' => $title,
                'text' => $text,
            ],
        ];
        if (!empty($atMobiles)) {
            $data['at']['atMobiles'] = $atMobiles;
        }
        if (!empty($atUserIds)) {
            $data['at']['atUserIds'] = $atUserIds;
        }
        if (true === $isAtAll) {
            $data['at']['isAtAll'] = $isAtAll;
        }
        return $this->sendMsg($data);
    }

    /**
     * @param $url
     * @param array $header
     * @param array $data
     * @param string $method
     * @param false $isSsl
     * @return mixed
     */
    protected function curlRequest($url, $header = [], $data = [], $method = 'POST', $isSsl = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if (strtoupper($method) === 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); // 在尝试连接时等待的秒数。设置为0，则无限等待。
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); // 设置 HTTP 头字段的数组。格式： array('Content-type: text/plain', 'Content-length: 100')
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // true 将curl_exec()获取的信息以字符串返回，而不是直接输出。
        // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
        if (false == $isSsl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data, true);
    }

}